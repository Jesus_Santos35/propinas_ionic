import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.page.html',
  styleUrls: ['./configuracion.page.scss'],
})
export class ConfiguracionPage implements OnInit {
  propina: number = 0;

  constructor(public navCtrl: NavController) { }

  Guardar(){
    let propina = String(this.propina);
    window.localStorage.setItem("porcentaje", propina);
    this.Navegar();
  }

  Navegar(){
    this.navCtrl.navigateForward("/");
  }

  ngOnInit() {
  }

  ionViewDidEnter(){

  }

}
